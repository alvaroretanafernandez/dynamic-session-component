import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionComponent } from './session/session.component';
import { SessionListComponent } from './session-list/session-list.component';

const routes: Routes = [
  { path: '', children: [
    { path: '',  component: SessionListComponent },
    { path: ':id', component: SessionComponent, pathMatch: 'full' },
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SessionRoutingModule { }
