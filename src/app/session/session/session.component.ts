import { AfterViewInit, Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { DynamicComponentService } from '../../shared/dynamic-component.service/dynamic-component.service';
import { ComponentOneComponent } from '../../shared/component-one/component-one.component';
import { ComponentTwoComponent } from '../../shared/component-two/component-two.component';
import { ComponentThreeComponent } from '../../shared/component-three/component-three.component';
import { ComponentFourComponent } from '../../shared/component-four/component-four.component';


@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css'],
  entryComponents: [
    ComponentOneComponent,
    ComponentTwoComponent,
    ComponentThreeComponent,
    ComponentFourComponent
  ]
})
export class SessionComponent implements OnInit, AfterViewInit, OnDestroy {
  currentSession: any;
  private sub: any;
  constructor(private viewContainerRef: ViewContainerRef,
              public route: ActivatedRoute,
              private dynamicComponentService: DynamicComponentService,
              private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.currentSession = params['id'];
    });
  }

  ngAfterViewInit() {

    const componentsToRender = environment.session[this.currentSession];
    console.log(componentsToRender);

    const containerRef = this.viewContainerRef;
    containerRef.clear();

    for ( let i = 0; i < componentsToRender.length; i++) {
      const component: any = this.dynamicComponentService.getComponent(componentsToRender[i]);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      containerRef.createComponent(componentFactory);
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
