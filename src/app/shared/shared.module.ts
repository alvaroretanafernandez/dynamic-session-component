import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentOneComponent } from './component-one/component-one.component';
import { ComponentTwoComponent } from './component-two/component-two.component';
import { ComponentThreeComponent } from './component-three/component-three.component';
import { ComponentFourComponent } from './component-four/component-four.component';
import { SharedLibModule } from './shared-lib.module';
import { DynamicComponentService } from './dynamic-component.service/dynamic-component.service';

@NgModule({
  imports: [
    CommonModule,
    SharedLibModule
  ],
  declarations: [
    ComponentOneComponent,
    ComponentTwoComponent,
    ComponentThreeComponent,
    ComponentFourComponent],
  exports: [
    SharedLibModule,
    ComponentOneComponent,
    ComponentTwoComponent,
    ComponentThreeComponent,
    ComponentFourComponent],
  providers: [DynamicComponentService]
})
export class SharedModule { }
