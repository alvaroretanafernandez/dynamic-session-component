import { Injectable } from '@angular/core';
import { ComponentOneComponent } from '../component-one/component-one.component';
import { ComponentTwoComponent } from '../component-two/component-two.component';
import { ComponentThreeComponent } from '../component-three/component-three.component';
import { ComponentFourComponent } from '../component-four/component-four.component';

@Injectable()
export class DynamicComponentService {

  constructor() {
  }

  getComponent(componentName: string) {

    if (componentName === 'ComponentOneComponent') {
      return ComponentOneComponent;
    }
    if (componentName === 'ComponentTwoComponent') {
      return ComponentTwoComponent;
    }

    if (componentName === 'ComponentThreeComponent') {
      return ComponentThreeComponent;
    }

    if (componentName === 'ComponentFourComponent') {
      return ComponentFourComponent;
    }
  }
}
